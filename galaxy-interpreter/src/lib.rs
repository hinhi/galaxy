use std::{collections::HashMap, error::Error, fmt, io::Read, ops::Deref, rc::Rc};

use Expr::*;

pub type Int = i64;

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Expr {
    // Primitive
    Ap(Rc<Expr>, Rc<Expr>),
    Num(Int),
    Cons,
    Nil,
    T,
    F,
    // Combinator
    B,
    C,
    S,
    I,
    // Unit argument function
    Car,
    Cdr,
    IsNil,
    Neg,
    // Bin argument function
    Add,
    Mul,
    Div,
    Eq,
    Lt,
    // Other
    Func(String),
    Ref(Rc<Expr>, usize),
    Pair(Rc<Expr>, Rc<Expr>),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum ParseError {
    UnexpectedEndOfLine,
    RemainToken,
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ParseError::UnexpectedEndOfLine => f.write_str("Unexpected EOL"),
            ParseError::RemainToken => f.write_str("Tokens remain"),
        }
    }
}

impl Error for ParseError {}

pub fn parse<'a, S: Into<&'a str>>(s: S) -> Result<Expr, ParseError> {
    let s = s.into();
    let mut tokens = s.split_ascii_whitespace();
    let res = consume_token(&mut tokens)?;
    if tokens.next().is_none() {
        Ok(res)
    } else {
        Err(ParseError::RemainToken)
    }
}

fn consume_token<'a, I>(tokens: &mut I) -> Result<Expr, ParseError>
where
    I: Iterator<Item = &'a str>,
{
    enum S {
        Ap0,
        Ap1(Expr),
    }
    let mut stack = Vec::new();
    loop {
        match tokens.next() {
            None => return Err(ParseError::UnexpectedEndOfLine),
            Some(s) => match s {
                "ap" => {
                    stack.push(S::Ap0);
                }
                s => {
                    let mut e = match s {
                        "cons" => Cons,
                        "nil" => Nil,
                        "t" => T,
                        "f" => F,
                        "b" => B,
                        "c" => C,
                        "s" => S,
                        "i" => I,
                        "car" => Car,
                        "cdr" => Cdr,
                        "isnil" => IsNil,
                        "neg" => Neg,
                        "add" => Add,
                        "mul" => Mul,
                        "div" => Div,
                        "eq" => Eq,
                        "lt" => Lt,
                        w => match w.parse() {
                            Ok(n) => Num(n),
                            Err(_) => Func(w.to_owned()),
                        },
                    };
                    loop {
                        match stack.pop() {
                            None => return Ok(e),
                            Some(S::Ap0) => {
                                stack.push(S::Ap1(e));
                                break;
                            }
                            Some(S::Ap1(f)) => e = Ap(f.into(), e.into()),
                        }
                    }
                }
            },
        }
    }
}

impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Ap(x, y) => f.write_fmt(format_args!("ap {} {}", x, y)),
            Num(n) => f.write_str(&n.to_string()),
            Cons => f.write_str("cons"),
            Nil => f.write_str("nil"),
            T => f.write_str("t"),
            F => f.write_str("f"),
            B => f.write_str("b"),
            C => f.write_str("c"),
            S => f.write_str("s"),
            I => f.write_str("i"),
            Car => f.write_str("car"),
            Cdr => f.write_str("cdr"),
            IsNil => f.write_str("isnil"),
            Neg => f.write_str("neg"),
            Add => f.write_str("add"),
            Mul => f.write_str("mul"),
            Div => f.write_str("div"),
            Eq => f.write_str("eq"),
            Lt => f.write_str("lt"),
            Func(name) => f.write_str(name),
            Ref(e, _) => f.write_fmt(format_args!("{}", e)),
            Pair(x, y) => f.write_fmt(format_args!("[{}, {}]", x, y)),
        }
    }
}

impl From<(Int, Int)> for Expr {
    fn from(pair: (Int, Int)) -> Expr {
        Ap(
            Ap(Cons.into(), Num(pair.0).into()).into(),
            Num(pair.1).into(),
        )
    }
}

pub struct Iter<'a> {
    expr: &'a Expr,
}

impl<'a> Iterator for Iter<'a> {
    type Item = &'a Expr;
    fn next(&mut self) -> Option<Self::Item> {
        match self.expr {
            Pair(arg1, arg2) => {
                self.expr = arg2;
                Some(arg1)
            }
            Ap(func1, arg1) => match func1.as_ref() {
                Ap(func2, arg2) => {
                    if let Cons = func2.as_ref() {
                        self.expr = arg1;
                        Some(arg2)
                    } else {
                        None
                    }
                }
                _ => None,
            },
            _ => None,
        }
    }
}

impl Expr {
    pub fn iter(&self) -> Iter {
        Iter { expr: self }
    }

    pub fn as_num(&self) -> Option<Int> {
        match self {
            Num(n) => Some(*n),
            _ => None,
        }
    }

    pub fn as_vec(&self) -> Option<(Int, Int)> {
        match self {
            Pair(x, y) => match (x.as_ref(), y.as_ref()) {
                (Num(a), Num(b)) => Some((*a, *b)),
                _ => None,
            },
            Ap(func1, arg1) => match func1.as_ref() {
                Ap(func2, arg2) => {
                    if let Cons = func2.as_ref() {
                        match (arg2.as_ref(), arg1.as_ref()) {
                            (Num(a), Num(b)) => Some((*a, *b)),
                            _ => None,
                        }
                    } else {
                        None
                    }
                }
                _ => None,
            },
            _ => None,
        }
    }

    pub fn replace_consts(&mut self, consts: &HashMap<String, Expr>) {
        match self {
            Ap(x, y) | Pair(x, y) => {
                Rc::make_mut(x).replace_consts(consts);
                Rc::make_mut(y).replace_consts(consts);
            }
            Func(name) => {
                if let Some(e) = consts.get(name) {
                    *self = e.clone();
                }
            }
            _ => (),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum EvalError {
    ExpectNum(Expr),
    UndefinedFunc(String),
    ZeroDivision,
}

impl fmt::Display for EvalError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            EvalError::ExpectNum(e) => {
                f.write_fmt(format_args!("Expected number, but got '{}'", e))
            }
            EvalError::UndefinedFunc(name) => {
                f.write_fmt(format_args!("Undefined function: {}", name))
            }
            EvalError::ZeroDivision => f.write_str("Division by zero"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Evaluator {
    pub functions: HashMap<String, Expr>,
}

impl Evaluator {
    pub fn new<N: Into<Option<HashMap<String, Expr>>>>(functions: N) -> Evaluator {
        let functions = functions.into();
        Evaluator {
            functions: functions.unwrap_or_else(HashMap::new),
        }
    }

    pub fn from_reader<R: Read>(input: &mut R) -> Result<Evaluator, ParseError> {
        let mut buf = Vec::new();
        input.read_to_end(&mut buf).unwrap();
        let mut functions = HashMap::new();
        for line in String::from_utf8(buf).unwrap().lines() {
            let mut words = line.split_ascii_whitespace();
            let name = words.next().unwrap();
            words.next().unwrap(); // skip `=`
            functions.insert(name.to_owned(), consume_token(&mut words)?);
        }
        Ok(Evaluator { functions })
    }

    pub fn optimized(&self) -> Evaluator {
        let mut consts = HashMap::new();
        let mut functions = HashMap::new();
        for name in self.functions.keys() {
            if let Ok(e) = self.eval(&Func(name.to_owned())) {
                match e {
                    Num(n) => consts.insert(name.to_owned(), Num(n)),
                    Cons => consts.insert(name.to_owned(), Cons),
                    _ => functions.insert(name.to_owned(), self.functions[name].clone()),
                };
            }
        }
        for func in functions.values_mut() {
            func.replace_consts(&consts);
        }
        Evaluator { functions }
    }

    pub fn std_func(&mut self) {
        self.functions.insert(
            "inc".to_owned(),
            consume_token(&mut "ap add 1".split_ascii_whitespace()).unwrap(),
        );
        self.functions.insert(
            "dec".to_owned(),
            consume_token(&mut "ap add -1".split_ascii_whitespace()).unwrap(),
        );
    }

    pub fn eval(&self, expr: &Expr) -> Result<Expr, EvalError> {
        let mut cache = Vec::new();
        self.eval_with_cache(expr, &mut cache)
    }

    pub fn eval_with_cache(
        &self,
        expr: &Expr,
        cache: &mut Vec<Option<Expr>>,
    ) -> Result<Expr, EvalError> {
        let mut expr = expr.clone();
        loop {
            let result = self.sub_eval(&expr, cache)?;
            if expr != result {
                expr = result;
            } else {
                return Ok(expr);
            }
        }
    }

    pub fn sub_eval(&self, expr: &Expr, cache: &mut Vec<Option<Expr>>) -> Result<Expr, EvalError> {
        match expr {
            Ref(e, id) => {
                if let Some(cached) = &cache[*id] {
                    Ok(cached.clone())
                } else {
                    let result = self.eval_with_cache(e, cache)?;
                    cache[*id] = Some(result.clone());
                    Ok(result)
                }
            }
            Func(name) => {
                if let Some(func) = self.functions.get(name) {
                    Ok(func.clone())
                } else {
                    Err(EvalError::UndefinedFunc(name.to_owned()))
                }
            }
            Ap(func1, arg1) => {
                let func1 = self.eval_with_cache(func1, cache)?;
                match &func1 {
                    Ap(func2, arg2) => match func2.as_ref() {
                        Cons => {
                            let arg2 = self.eval_with_cache(arg2, cache)?;
                            let arg1 = self.eval_with_cache(arg1, cache)?;
                            Ok(Pair(arg2.into(), arg1.into()))
                        }
                        Ap(func3, arg3) => match func3.as_ref() {
                            S => {
                                let id = cache.len();
                                cache.push(None);
                                Ok(Ap(
                                    Ap(arg3.clone(), Ref(arg1.clone(), id).into()).into(),
                                    Ap(arg2.clone(), Ref(arg1.clone(), id).into()).into(),
                                ))
                            }
                            C => Ok(Ap(Ap(arg3.clone(), arg1.clone()).into(), arg2.clone())),
                            B => Ok(Ap(arg3.clone(), Ap(arg2.clone(), arg1.clone()).into())),
                            Cons => Ok(Ap(Ap(arg1.clone(), arg3.clone()).into(), arg2.clone())),
                            _ => unreachable!(),
                        },
                        T => Ok(arg2.deref().clone()),
                        F => Ok(arg1.deref().clone()),
                        Add => {
                            let (left, right) = self.expect_num(arg2, arg1, cache)?;
                            Ok(Num(left + right))
                        }
                        Mul => {
                            let (left, right) = self.expect_num(arg2, arg1, cache)?;
                            Ok(Num(left * right))
                        }
                        Div => {
                            let (left, right) = self.expect_num(arg2, arg1, cache)?;
                            if right == 0 {
                                Err(EvalError::ZeroDivision)
                            } else {
                                Ok(Num(left / right))
                            }
                        }
                        Eq => {
                            let (left, right) = self.expect_num(arg2, arg1, cache)?;
                            if left == right {
                                Ok(T)
                            } else {
                                Ok(F)
                            }
                        }
                        Lt => {
                            let (left, right) = self.expect_num(arg2, arg1, cache)?;
                            if left < right {
                                Ok(T)
                            } else {
                                Ok(F)
                            }
                        }
                        _ => Ok(Ap(func1.into(), arg1.clone())),
                    },
                    I => Ok(self.eval_with_cache(arg1, cache)?),
                    Car => match arg1.as_ref() {
                        Pair(a, _) => Ok(a.deref().clone()),
                        _ => Ok(Ap(arg1.clone(), T.into())),
                    },
                    Cdr => match arg1.as_ref() {
                        Pair(_, a) => Ok(a.deref().clone()),
                        _ => Ok(Ap(arg1.clone(), F.into())),
                    },
                    IsNil => {
                        if let Nil = arg1.as_ref() {
                            Ok(T)
                        } else {
                            Ok(Ap(
                                arg1.clone(),
                                Ap(T.into(), Ap(T.into(), F.into()).into()).into(),
                            ))
                        }
                    }
                    Neg => {
                        let right1 = self.eval_with_cache(arg1, cache)?;
                        if let Num(n) = right1 {
                            Ok(Num(-n))
                        } else {
                            Err(EvalError::ExpectNum(right1))
                        }
                    }
                    Nil => Ok(T),
                    Pair(arg3, arg2) => Ok(Ap(Ap(arg1.clone(), arg3.clone()).into(), arg2.clone())),
                    _ => Ok(Ap(func1.into(), arg1.clone())),
                }
            }
            _ => Ok(expr.clone()),
        }
    }

    fn expect_num(
        &self,
        left: &Expr,
        right: &Expr,
        cache: &mut Vec<Option<Expr>>,
    ) -> Result<(Int, Int), EvalError> {
        let left = self.eval_with_cache(left, cache)?;
        if let Num(left) = left {
            let right = self.eval_with_cache(right, cache)?;
            if let Num(right) = right {
                Ok((left, right))
            } else {
                Err(EvalError::ExpectNum(right))
            }
        } else {
            Err(EvalError::ExpectNum(left))
        }
    }
}

pub type InteractResult = Result<Option<(Expr, Vec<Vec<(Int, Int)>>)>, EvalError>;

pub fn interact<S: Into<String>>(
    evaluator: &Evaluator,
    main: S,
    state: Expr,
    event: (Int, Int),
) -> InteractResult {
    let event: Expr = event.into();
    let expr = Expr::Ap(
        Expr::Ap(Expr::Func(main.into()).into(), state.into()).into(),
        event.into(),
    );
    let result = evaluator.eval(&expr)?;
    let mut iter = result.iter();
    let flag = iter.next().unwrap().as_num().unwrap();
    let state = iter.next().unwrap();
    let images = {
        let mut images = Vec::new();
        let data = iter.next().unwrap();
        for img in data.iter() {
            let mut image = Vec::new();
            for vec in img.iter() {
                image.push(vec.as_vec().unwrap());
            }
            images.push(image);
        }
        images
    };
    if flag == 0 {
        Ok(Some((state.clone(), images)))
    } else {
        Ok(None)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn eval(s: &str) -> Expr {
        let mut evaluator = Evaluator::new(None);
        evaluator.std_func();
        let expr = parse(s).unwrap();
        assert_eq!(format!("{}", expr), s);
        evaluator.eval(&expr).unwrap()
    }

    #[test]
    fn neg() {
        assert_eq!(eval("ap neg 0"), Num(0));
        assert_eq!(eval("ap neg 1"), Num(-1));
        assert_eq!(eval("ap neg -2"), Num(2));
    }

    #[test]
    fn add() {
        assert_eq!(eval("ap ap add 1 2"), Num(3));
        assert_eq!(eval("ap ap add 3 1"), Num(4));
        assert_eq!(eval("ap ap add 0 -5"), Num(-5));
    }

    #[test]
    fn mul() {
        assert_eq!(eval("ap ap mul 4 -2"), Num(-8));
        assert_eq!(eval("ap ap mul 0 1"), Num(0));
    }

    #[test]
    fn div() {
        assert_eq!(eval("ap ap div 4 2"), Num(2));
        assert_eq!(eval("ap ap div 4 3"), Num(1));
        assert_eq!(eval("ap ap div 4 4"), Num(1));
        assert_eq!(eval("ap ap div 4 5"), Num(0));
        assert_eq!(eval("ap ap div 5 2"), Num(2));
        assert_eq!(eval("ap ap div 6 -2"), Num(-3));
        assert_eq!(eval("ap ap div 5 -3"), Num(-1));
        assert_eq!(eval("ap ap div -5 3"), Num(-1));
        assert_eq!(eval("ap ap div -5 -3"), Num(1));
    }

    #[test]
    fn eq() {
        assert_eq!(eval("ap ap eq 0 1"), F);
        assert_eq!(eval("ap ap eq 0 0"), T);
        assert_eq!(eval("ap ap eq 2 2"), T);
    }

    #[test]
    fn lt() {
        assert_eq!(eval("ap ap lt 0 -1"), F);
        assert_eq!(eval("ap ap lt 0 0"), F);
        assert_eq!(eval("ap ap lt 0 1"), T);
        assert_eq!(eval("ap ap lt -19 -20"), F);
        assert_eq!(eval("ap ap lt -21 -20"), T);
    }

    #[test]
    fn s_comb() {
        assert_eq!(eval("ap ap ap s add inc 1"), Num(3));
        assert_eq!(eval("ap ap ap s mul ap add 1 6"), Num(42));
    }

    #[test]
    fn c_comb() {
        assert_eq!(eval("ap ap ap c 1 2 3"), parse("ap ap 1 3 2").unwrap());
        assert_eq!(eval("ap ap ap c div 1 2"), Num(2));
    }

    #[test]
    fn b_comb() {
        assert_eq!(eval("ap ap ap c 1 2 3"), parse("ap ap 1 3 2").unwrap());
        for i in -10..=10 {
            assert_eq!(eval(&format!("ap ap ap b inc dec {}", i)), Num(i));
        }
    }

    #[test]
    fn i_comb() {
        assert_eq!(eval("ap i 0"), Num(0));
    }

    #[test]
    fn t() {
        assert_eq!(eval("ap ap t 1 2"), Num(1));
        assert_eq!(eval("ap ap t t i"), T);
        assert_eq!(eval("ap ap t t ap inc 5"), T);
        assert_eq!(eval("ap ap t ap inc 5 t"), Num(6));
    }

    #[test]
    fn f() {
        assert_eq!(eval("ap ap f 1 2"), Num(2));
    }

    #[test]
    fn car() {
        assert_eq!(eval("ap car ap ap cons 0 ap ap cons 1 nil"), Num(0));
    }

    #[test]
    fn cdr() {
        assert_eq!(
            eval("ap cdr ap ap cons 0 ap ap cons 1 nil"),
            Pair(Num(1).into(), Nil.into())
        );
    }

    #[test]
    fn is_nil() {
        assert_eq!(eval("ap isnil nil"), T);
        assert_eq!(eval("ap isnil ap ap cons 0 1"), F);
    }

    #[test]
    fn internal_list() {
        assert_eq!(
            eval("ap ap cons ap inc 1 ap ap cons ap dec 5 nil"),
            Pair(Num(2).into(), Pair(Num(4).into(), Nil.into()).into())
        );
    }

    #[test]
    fn parse_error() {
        assert_eq!(parse("1 2"), Err(ParseError::RemainToken));
        assert_eq!(parse("ap 1"), Err(ParseError::UnexpectedEndOfLine));
    }

    #[test]
    fn eval_error() {
        let mut evaluator = Evaluator::new(None);
        evaluator.std_func();

        let expr = parse("ap ap div 3 0").unwrap();
        assert_eq!(evaluator.eval(&expr), Err(EvalError::ZeroDivision));

        let expr = parse("ap ap add 1 t").unwrap();
        assert_eq!(evaluator.eval(&expr), Err(EvalError::ExpectNum(T)));
        let expr = parse("ap ap add f t").unwrap();
        assert_eq!(evaluator.eval(&expr), Err(EvalError::ExpectNum(F)));
        let expr = parse("ap neg nil").unwrap();
        assert_eq!(evaluator.eval(&expr), Err(EvalError::ExpectNum(Nil)));

        let expr = parse("ap func nil").unwrap();
        assert_eq!(
            evaluator.eval(&expr),
            Err(EvalError::UndefinedFunc("func".to_owned()))
        );

        let expr = parse("ap ap cons ap inc t nil").unwrap();
        assert_eq!(evaluator.eval(&expr), Err(EvalError::ExpectNum(T)));
        let expr = parse("ap ap cons 1 ap inc t").unwrap();
        assert_eq!(evaluator.eval(&expr), Err(EvalError::ExpectNum(T)));
    }

    #[test]
    fn many_ap() {
        assert_eq!(
            eval("ap ap ap ap ap cons neg nil t 1 2"),
            Ap(Num(-1).into(), Num(2).into())
        );
    }

    #[test]
    fn from_reader() {
        let s = r#"add2 = ap add 2
mul5 = ap mul 5
main = ap add2 ap mul5 3"#;
        let evaluator = Evaluator::from_reader(&mut s.as_bytes()).unwrap();
        assert_eq!(evaluator.eval(&parse("main").unwrap()).unwrap(), Num(17));
    }

    #[test]
    fn iter() {
        let expr = parse("ap ap cons ap ap cons 1 nil ap ap cons 2 ap ap cons 3 nil").unwrap();
        let mut it = expr.iter();
        let mut it0 = it.next().unwrap().iter();
        assert_eq!(it0.next(), Some(&Num(1)));
        assert_eq!(it0.next(), None);
        assert_eq!(it.next(), Some(&Num(2)));
        assert_eq!(it.next(), Some(&Num(3)));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn from_pair() {
        let expr: Expr = (1, 2).into();
        let (a, b) = expr.as_vec().unwrap();
        assert_eq!(a, 1);
        assert_eq!(b, 2);
    }

    #[test]
    fn stateless_draw() {
        let s = "stateless = ap ap c ap ap b b ap ap b ap b ap cons 0 \
        ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil";
        let eval = Evaluator::from_reader(&mut s.as_bytes()).unwrap();
        let mut state = Nil;
        for x in -10..=10 {
            for y in -10..=10 {
                let (new_state, images) = interact(&eval, "stateless", state, (x, y))
                    .unwrap()
                    .unwrap();
                state = new_state;
                assert_eq!(images, vec![vec![(x, y)]]);
            }
        }
    }

    #[test]
    fn stateful_draw() {
        let s = "stateful = ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c \
        ap ap b b cons ap ap c cons nil ap ap c cons nil ap c cons";
        let eval = Evaluator::from_reader(&mut s.as_bytes()).unwrap();
        let mut state = Nil;
        let mut v = Vec::new();
        for x in -3..=3 {
            for y in -3..=3 {
                v.insert(0, (x, y));
                let (new_state, images) =
                    interact(&eval, "stateful", state, (x, y)).unwrap().unwrap();
                state = new_state;
                assert_eq!(images.len(), 1);
                assert_eq!(images[0], v);
            }
        }
    }

    #[test]
    fn galaxy() {
        let s = include_str!("../../galaxy.txt");
        assert!(Evaluator::from_reader(&mut s.as_bytes()).is_ok());
    }

    #[test]
    fn optimized() {
        let s = r#":1 = 1
:2 = cons
:3 = ap neg 3
:4 = ap ap cons ap ap cons :1 :2 :3
"#;
        let eval = Evaluator::from_reader(&mut s.as_bytes()).unwrap();
        let eval = eval.optimized();
        assert_eq!(eval.functions.len(), 1);
        assert_eq!(
            eval.functions.get(":4"),
            Some(&parse("ap ap cons ap ap cons 1 cons -3").unwrap())
        );
    }
}
