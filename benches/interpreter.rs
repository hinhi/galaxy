use criterion::{criterion_group, criterion_main, Criterion};

use galaxy_interpreter::{interact, parse, Evaluator};

static GALAXY: &str = include_str!("../galaxy.txt");

fn parse_galaxy(c: &mut Criterion) {
    c.bench_function("parse_galaxy", |b| {
        b.iter(|| Evaluator::from_reader(&mut GALAXY.as_bytes()).unwrap());
    });
}

fn eval_galaxy_lightweight(c: &mut Criterion) {
    let e = Evaluator::from_reader(&mut GALAXY.as_bytes())
        .unwrap()
        .optimized();
    let state =
        parse("ap ap cons 0 ap ap cons ap ap cons 3 nil ap ap cons 0 ap ap cons nil nil").unwrap();
    c.bench_function("eval_galaxy_lightweight", |b| {
        b.iter(|| interact(&e, "galaxy", state.clone(), (1, 0)).unwrap());
    });
}

fn eval_galaxy_heavy(c: &mut Criterion) {
    let e = Evaluator::from_reader(&mut GALAXY.as_bytes())
        .unwrap()
        .optimized();
    let state =
        parse("ap ap cons 1 ap ap cons ap ap cons 11 nil ap ap cons 0 ap ap cons nil nil").unwrap();
    c.bench_function("eval_galaxy_heavy", |b| {
        b.iter(|| interact(&e, "galaxy", state.clone(), (13, 4)).unwrap());
    });
}

criterion_group!(
    benches,
    parse_galaxy,
    eval_galaxy_lightweight,
    eval_galaxy_heavy,
);
criterion_main!(benches);
