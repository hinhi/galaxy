.PHONY: test build serve bench

test:
	cargo test --all

build:
	wasm-pack build --target web --out-name wasm --out-dir ./static/bin

serve: build
	miniserve ./static --index index.html

bench: test
	cargo bench --bench interpreter -- --sample-size 10 -b master
