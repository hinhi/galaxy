#![recursion_limit = "1024"]
use galaxy_interpreter::{interact, Evaluator, Expr, Int};
use web_sys::MouseEvent;
use yew::prelude::*;

type Image = Vec<Vec<(Int, Int)>>;

struct Model {
    evaluator: Evaluator,
    state: Expr,
    image: Image,
    history: Vec<(Expr, (Int, Int))>,
}

enum Msg {
    CellClick(Int, Int),
    HistClick(usize),
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        let evaluator = Evaluator::from_reader(&mut include_str!("../galaxy.txt").as_bytes())
            .unwrap()
            .optimized();
        let (state, image) = interact(&evaluator, "galaxy", Expr::Nil, (0, 0))
            .unwrap()
            .unwrap();
        Self {
            evaluator,
            state,
            image,
            history: vec![(Expr::Nil, (0, 0))],
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::CellClick(x, y) => {
                let (state, image) =
                    interact(&self.evaluator, "galaxy", self.state.clone(), (x, y))
                        .unwrap()
                        .unwrap();
                if state != self.state || image != self.image {
                    self.history.push((self.state.clone(), (x, y)));
                    self.state = state;
                    self.image = image;
                    true
                } else {
                    false
                }
            }
            Msg::HistClick(i) => {
                self.history.drain(i..);
                if self.history.is_empty() {
                    self.history.push((Expr::Nil, (0, 0)));
                }
                let (state, image) = interact(
                    &self.evaluator,
                    "galaxy",
                    self.history.last().unwrap().0.clone(),
                    self.history.last().unwrap().1.clone(),
                )
                .unwrap()
                .unwrap();
                self.state = state;
                self.image = image;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link();
        let mut cell_callback =
            |x: Int, y: Int| link.callback(move |_: MouseEvent| Msg::CellClick(x, y));
        let hint = match format!("{}", self.state).as_str() {
            "[1, [[9, nil], [0, [nil, nil]]]]" => Some((9, -3)),
            "[1, [[10, nil], [0, [nil, nil]]]]" => Some((-4, 10)),
            "[1, [[11, nil], [0, [nil, nil]]]]" => Some((13, 4)),
            _ => None,
        };
        html! {
            <div>
                { grid(&self.image, &mut cell_callback, hint) }
                <p>{format!("state: {}", self.state)}</p>
                <ul>
                    {
                        self.history.iter().enumerate().rev().map(|(i, (state, event))| {
                            html! {
                                <li>
                                    <button onclick={link.callback(move |_: MouseEvent| Msg::HistClick(i))}>{"re"}</button>
                                    {state}
                                    {format!(" → {:?}", event)}
                                </li>
                            }
                        }).collect::<Html>()
                    }
                </ul>
            </div>
        }
    }
}

fn grid<F>(image: &Image, f: &mut F, hint: Option<(Int, Int)>) -> Html
where
    F: FnMut(Int, Int) -> Callback<MouseEvent>,
{
    let mut width_pt = 800;
    let mut height_pt = 800;
    let margin_n = 2;
    let colors = ["#fcfeff", "#24dcff", "#fc24ff", "#fcfe24", "#2424ff"];

    let mut min_x = Int::MAX;
    let mut min_y = Int::MAX;
    let mut max_x = Int::MIN;
    let mut max_y = Int::MIN;

    let mut cells = Vec::new();
    for (i, row) in image.iter().enumerate() {
        for &(x, y) in row.iter() {
            min_x = x.min(min_x);
            min_y = y.min(min_y);
            max_x = x.max(max_x);
            max_y = y.max(max_y);
            cells.push((y, x, i));
        }
    }
    let length_x = max_x - min_x + 1 + margin_n * 2;
    let length_y = max_y - min_y + 1 + margin_n * 2;
    let mut cell_pt = std::cmp::min(width_pt / length_x, height_pt / length_y);
    if cell_pt < 4 {
        cell_pt = 4;
        width_pt = length_x * cell_pt;
        height_pt = length_y * cell_pt;
    }
    cells.sort();

    fn s(v: i64) -> String {
        v.to_string()
    }

    html! (
        <svg width={s(width_pt)} height={s(height_pt)} xmlns="http://www.w3.org/2000/svg">
            <rect x=0 y=0 width={s(width_pt)} height={s(height_pt)} fill="#2c3e50" />
            {
                cells.into_iter().map(|(y, x, i)| {
                    let fill = colors[i % colors.len()];
                    let r = if Some((x, y)) == hint {
                        cell_pt / 5
                    } else {
                        0
                    };
                    html! {<>
                        <rect
                         x={s((x + margin_n - min_x) * cell_pt)}
                         y={s((y + margin_n - min_y) * cell_pt)}
                         rx={s(r)}
                         ry={s(r)}
                         width={s(cell_pt)}
                         height={s(cell_pt)}
                         fill={fill}
                         onclick={f(x, y)} />
                        <text
                         x={s((x + margin_n - min_x) * cell_pt + 1)}
                         y={s((y + margin_n - min_y) * cell_pt + 1)}
                         class="pos">{format!("({}, {})", x, y)}</text>
                    </>}
                }).collect::<Html>()
            }
        </svg>
    )
}

fn main() {
    yew::start_app::<Model>();
}
