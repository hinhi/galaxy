#!/bin/sh

set -eu

cd "$(dirname "$0")"

export CARGO_INCREMENTAL=0
export RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Copt-level=0 -Clink-dead-code -Coverflow-checks=off -Zpanic_abort_tests -Cpanic=abort"
export RUSTDOCFLAGS="-Cpanic=abort"

rm -rf ./target/debug/deps/ ./target/debug/coverage/

cargo +nightly build
cargo +nightly test --all
grcov ./target/debug/ -s . -t html --llvm --branch --ignore-not-existing -o ./target/debug/coverage/
